
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import JobList from './Components/JobList/JobList';
import JobDetails from './Components/JobDetails/JobDetails';
import PageNotFound from './Components/PageNotFound/PageNotFound';

function App() {
  return (
    <div className="App">
          <Router>
          <Header />
          <div className="main">
          <Switch>
            <Route exact path="/" component={JobList} /> 
            <Route exact path="/jobs-detail/:id/:imgId" component={JobDetails} />   
            <Route path='*'  component={PageNotFound} /> 
          </Switch>
          <Footer />
          </div>

         
        </Router>




    </div>
  );
}

export default App;
