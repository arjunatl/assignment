import React, { useState, useEffect } from "react";
import "./JobDetails.scss";
import axios from "axios";
import Loader from '../Shared/Loader/Loader';
import {image} from '../Shared/imageData';
export default function JobDetails(props) {
  console.log(props.match.params.id);
  const [job, setJob] = useState();
  const [loader, setLoader] = useState(false);  

  useEffect(() => {
    setLoader(true);
    axios
      .get(`${process.env.REACT_APP_BASE_URL}jobs/${props.match.params.id}`)
      .then((response) => {
        setJob(response.data);
        setLoader(false);
      })
      .catch(function (error) {
        // handle error
        console.log(error);
        setLoader(false);
      });
  }, []);
  return (      
    <div className="container detail-container">
    {loader && <Loader/>}
      <div className="details_wrapper">
        <div className="img-profile">
          <img
            src={`${image[props.match.params.imgId]}`}
            alt="logo"
          />
        </div>
        <div className="row">
          <div className="col pr-0">
            <div className="block-about two bg-grey-2 h-100">
              <div className="block-title">
                <h2 className="title">Job Info</h2>
              </div>

              <ul className="mt-40 info">
                <li>
                  <span>Job Name</span> : {job?.title}
                </li>
                <li>
                  <span>Job Code</span> : {job?.uuid}
                </li>
                <li>
                  <span>Phone</span> : + 123-456-789-456
                </li>
                <li>
                  <span>Email</span> : support@mutationmedia.net
                </li>
                <li>
                  <span>Skype</span> : Johanson_Doe{" "}
                </li>
                <li>
                  <span>Freelance</span> : Available
                </li>
                <li>
                  <span>Adresse</span> : 1234 Street Road, City Name, IN 567
                  890.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
