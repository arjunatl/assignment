import React from "react";
import "./PageNotFound.scss";
export default function PageNotFound() {
  return (
    <div className="pagenotfound">
    <img src="https://drudesk.com/sites/default/files/2018-02/404-error-page-not-found.jpg" alt="page not found" />
    </div>)

}