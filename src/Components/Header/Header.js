import React from "react";
import "./Header.scss";
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from "react-router-dom";
export default function Header() {
  return (
<Navbar expand="lg" className="w-100">
  <div className="container">
  <Link className="logo" to="/">JobEasy</Link>
  <Navbar.Toggle aria-controls="basic-navbar-nav" />
  <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
    <Nav className="mr-auto">
      <NavLink to="/">Home</NavLink>
      <Nav.Link href="#link">About</Nav.Link>
      <Nav.Link href="#link">
        <button className="login">Login</button>
      </Nav.Link>
      <Nav.Link href="#link">
      <button className="register">Register</button>
      </Nav.Link>
    </Nav>
    
  </Navbar.Collapse>
  </div>
</Navbar>
  );
}
