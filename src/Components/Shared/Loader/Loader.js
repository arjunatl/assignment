import React from "react";
import "./Loader.scss";
export default function Loader() {
  return (
    <div id="overlay">
    <div className="w-100 d-flex justify-content-center align-items-center">
      <div className="spinner"></div>
    </div>
  </div>)

}