import React, { useState, useEffect } from "react";
import "./JobList.scss";
import { Link } from "react-router-dom";
import axios from "axios";
import Loader from '../Shared/Loader/Loader';
import {image} from '../Shared/imageData';
export default function JobList() {
  const [jobs, setJobs] = useState([]);
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    setLoader(true);
    axios
      .get(`${process.env.REACT_APP_BASE_URL}jobs`)
      .then((response) => {
        setLoader(false)
        setJobs(response.data);
      })
      .catch(function (error) {
        // handle error
        setLoader(false)
        console.log(error);
      });
  }, []);
  return (
  
    <div className="job-container">
        {loader && <Loader/>}
      <div className="container">
        <div className="row">
          {jobs.map((job,index) => (
            <div className="col-md-4" key={index}>
              <Link to={`/jobs-detail/${job.uuid}/${index}`}>
              <div className="job-wrapper">
                <div className="img-title-wrap">
                  <img
                    alt="sample-logo"
                    src={`${image[index]}`}
                  />
                  <div className="title-loc">
                    <h5 className="mt-2">
                      {job.title}
                      <i className="fa fa-heart-o" aria-hidden="true"></i>
                    </h5>
                    <h6 className="mt-2">Technopark,Trivandrum</h6>
                  </div>
                </div>
                <p>
                  What is Lorem Ipsum Lorem Ipsum is simply dummy text of the
                  printing and typesetting industry Lorem Ipsum has been the
                  industry's standard dummy text ever since the 1500s when an
                  unknown printer took a galley of type and scrambled it to make
                  a type specimen book it has
                </p>
                <div className="tags position-relative">
                  <ul>
                    <li>Full Time</li>
                    <li>Min 1 Year</li>
                    <li>Senior Level</li>
                  </ul>
                  <span>
                    New <i> 3d</i>
                  </span>
                </div>

                <div className="row btn-wrap">
                  <div className="col">
                    <button className="apply">Apply</button>
                  </div>
                  <div className="col">
                    <button className="message col">Messages</button>
                  </div>
                </div>
              </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
